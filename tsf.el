;;; tsf.el --- TagSpaces-esque tags on files -*- lexical-binding: t -*-

;; Author: Kisaragi Hiu <mail@kisaragi-hiu.com>
;; Version: 0.0.1
;; Package-Requires: ((emacs "26.1"))
;; Keywords: convenience


;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; tst.el's functions applied to real files.

;;; Code:

(require 'tst)
(require 'f)
(require 'dash)

(defun tsf/ls (dir tag)
  "List files tagged with TAG under DIR."
  (cl-loop for f in (f-entries dir)
           when (member tag (tst-get f))
           collect f))

(defun tsf/add (file tag)
  "Add TAG to FILE's name."
  (let ((newname (--> file
                   f-no-ext
                   (tst-add it tag)
                   (concat it "." (f-ext file)))))
    (f-move file newname)
    newname))

(defun tsf/remove (file tag)
  "Remove TAG from FILE's name."
  (let ((newname (--> file
                   f-no-ext
                   (tst-remove it tag)
                   (concat it "." (f-ext file)))))
    (f-move file newname)
    newname))

(defun tsf/set (file tags)
  "Set FILE's tag list to TAGS."
  (let ((newname (--> file
                   f-no-ext
                   (tst-set it tags)
                   (concat it "." (f-ext file)))))
    (f-move file newname)
    newname))

(provide 'tsf)

;;; tsf.el ends here
