;;; tst-test.el --- Tests for tst.el -*- lexical-binding: t; -*-

(when (require 'undercover nil t)
  (undercover "*.el"))

(require 'ert)
(require 'tst)

(ert-deftest tst-get ()
  ;; Empty
  (should (null (tst-get "abc[]")))
  (should (null (tst-get "abc")))
  ;; Not at end of string
  (should (null (tst-get "abc[a b]def")))
  ;; Only grabs the last bracket
  (should (null
           (seq-difference '("tag1" "tag2")
                           (tst-get "abc[a b]def[tag1 tag2]"))))
  ;; Normal
  (should (null
           (seq-difference '("a" "b")
                           (tst-get "abc[a b]")))))
(ert-deftest tst-clear ()
  (should (equal (tst-clear "abc[tag1 tag2]")
                 "abc"))
  (should (equal (tst-clear "abc[def]g[tag1 tag2]")
                 "abc[def]g"))
  (should (equal (tst-clear "abc")
                 "abc")))
(ert-deftest tst-set ()
  ;; Normal setting
  (should (equal (tst-set "abc" '("a" "b" "c" "d" "30"))
                 "abc[a b c d 30]"))
  ;; Overwrite existing value
  (should (equal (tst-set "abc[d e f]" '("g" "h" "i"))
                 "abc[g h i]"))
  ;; Set to empty -> don't leave behind "[]"
  (should (equal (tst-set "abc[d e f]" '())
                 "abc")))
(ert-deftest tst-add ()
  ;; Just adding
  (should (equal (tst-add "abc" "tag1")
                 "abc[tag1]"))
  ;; Add to end
  (should (equal (tst-add "abc[tag1]" "tag2")
                 "abc[tag1 tag2]"))
  ;; No repeats
  (should (equal (tst-add "abc[tag1]" "tag1")
                 "abc[tag1]")))
(ert-deftest tst-remove ()
  ;; Remove last element
  (should (equal (tst-remove "abc[def]" "def")
                 "abc"))
  ;; Remove one of many
  (should (equal (tst-remove "abc[def ghi]" "def")
                 "abc[ghi]"))
  ;; Remove nonexistent
  (should (equal (tst-remove "abc" "def")
                 "abc")))

(provide 'tst-tests)
;;; tst-test.el ends here
