;;; tst.el --- TagSpaces-esque tags in strings  -*- lexical-binding: t; -*-

;; Authors: Kisaragi Hiu <mail@kisaragi-hiu.com>
;; URL: https://gitlab.com/kisaragi-hiu/tst.el
;; Version: 0.1.0
;; Package-Requires: ((emacs "25.1") (s "1.2.0") (dash "2.17.0"))
;; Keywords: lisp

;;; Commentary:

;; Storing tags in strings in a way inspired by TagSpaces.
;;
;; Syntax:
;;
;; <the rest of the string>[tag1 tag2 tag3...]<end of string>
;;
;; Nuances:
;; - Tags cannot contain spaces.
;;   As I want these tags to be used in filenames, we cannot use
;;   double quotes or backslash to escape the separator.
;;   It's just not worth the trouble to allow whitespaces when we
;;   don't even have access to the most widespread escaping syntax.
;; - Try to avoid an empty tag list (like "content[]") when returning values
;; - Maybe try to output as-is if tag list is unchanged (so
;;   (tst-remove "content[]" "not there") -> "content[]")
;;   This kind of conflicts with the first rule.

;;; Code:

(require 'dash)
(require 's)

(defun tst-get (str)
  "Get tag list from STR."
  (-some--> (s-match
             ;; Without the first greedy match (* any), the first "["
             ;; in the string would match first, then the eos would
             ;; drag the "]" to the end.
             ;; (s-match (rx "[" (*? any) "]" eos) "ab[c]d[e]")
             ;;   -> ("[c]d[e]"), not ("[e]").
             (rx (* any) "[" (group (*? any)) "]" eos)
             str)
    cadr
    (s-split " " it t)))

(defun tst-clear (str)
  "Clear all of STR's tags.
Example: abc[tag1 tag2] -> abc"
  (or (cadr (s-match (rx (group (* any)) "[" (*? any) "]" eos) str))
      str))

(defun tst-set (str tags)
  "Set tag list to TAGS in STR, throwing away existing values."
  (let ((cleared (tst-clear str)))
    (if tags
        (format "%s[%s]" cleared (s-join " " tags))
      ;; Prefer to not return "abc[]"
      cleared)))

(defun tst-add (str tag)
  "Add TAG to STR's tag list."
  (let ((existing (tst-get str)))
    (if (member tag existing)
        str
      (tst-set str (-snoc existing tag)))))

(defun tst-remove (str tag)
  "Remove TAG from STR's tag list."
  (tst-set str (remove tag (tst-get str))))

(provide 'tst)
;;; tst.el ends here
